#!/usr/bin/env python
# -*- coding: utf-8 -*-

from trajetorias import trajetorias_iniciais, d, saida
from latlg_to_utm import *
from auxiliar import *
import time
import csv
from trajetoria import *


def prepara_base(trajetorias_iniciais):
    trajs = []
    #trajs = {}

    i = 0
    for t in trajetorias_iniciais:
        ti = clean_coords(t)
        trajetoria = lat_to_utm(ti)    
        tamanho_trajetoria = len(trajetoria) #tamanho de cada trajetória, ou seja, quantidade de pontos de cada uma
        trajetoria = Trajetoria(i, trajetoria, tamanho_trajetoria)
        #trajs.append((i, trajetoria, tamanho_trajetoria))
        trajs.append(trajetoria)
        i += 1
    
    return trajs    

def imprime_trajs(trajs):   
    for i in trajs:
        print(i.id, i.trajetoria, i.tamanho_trajetoria)

def total_pontos_trajetorias_individuais(trajs):
    for i in trajs:
        #print('Trajetória %d tem %d pontos.' %(i[0],i[2]))
        print('Trajetória %d tem %d pontos.' %(i.id, i.tamanho_trajetoria))

def total_pontos_base(trajs):
    total_pontos = 0
    
    for i in trajs:
        #total_pontos += i[2]
        total_pontos += i.tamanho_trajetoria
    return total_pontos

def total_trajetorias_base(trajs):
    return len(trajs)
    
def remove_candidatos(candidatos):
    for c in candidatos:
        if len(c.vizinhos) == 0:
            candidatos.remove(c)
        print('ponto: ' + str(c.pto_candidato.x) + ',' + str(c.pto_candidato.y) + ' c.vizinhos: ' + str(len(c.vizinhos)))
        
    return candidatos

def imprime_candidatos(candidatos):
    for c in candidatos:
        print('ponto: ' + str(c.pto_candidato.x) + ',' + str(c.pto_candidato.y) + ' c.vizinhos: ' + str(len(c.vizinhos)))

def output_file_traclus(arquivo, candidatos):
    '''[{"y": -48.612627, "x": -26.230151000000003}, {"y": -48.61032057292498, "x": -26.231111947737364}]'''
    arquivo = open(arquivo, 'w')
    coordenadas = {}
    latlon = []
    texto = []
    for c in candidatos:
        
        c_latlon = utm_to_lat(c.pto_candidato)
        for v in c.vizinhos:
            coordenada = utm_to_lat(v[1])
            coordenadas["x"], coordenadas["y"] = coordenada[0], coordenada[1]
            latlon.append(coordenadas)
        texto.append(latlon)        
    arquivo.write(str(texto))
        
        

def output_file(arquivo, candidatos):
    with open(arquivo, 'w') as saida:
        latlon = []
        for c in candidatos:
            c_latlon = utm_to_lat(c.pto_candidato)
            for v in c.vizinhos:                
                #latlon.append(utm_to_lat(v))
                latlon.append((v[0],utm_to_lat(v[1])))

            fieldnames = ['candidato', 'vizinhos']
            writer = csv.DictWriter(saida, fieldnames=fieldnames)

            writer.writeheader()
            writer.writerow({'candidato': c_latlon, 'vizinhos': latlon})


def main():
    inicio = time.time()
    
    trajs = prepara_base(trajetorias_iniciais)
    
    
#   imprime_trajs(trajs)
    
    
    total_trajs = total_trajetorias_base(trajs)
    total_pontos = total_pontos_base(trajs)
    total_pontos_trajetorias_individuais(trajs) 
    print('quantidade total de trajs:' + str(total_trajs))
    print('quantidade total de pontos:' + str(total_pontos))
      
    
    candidatos = anota(d, trajs)
    print('pontos candidatos em primeira passada: ' + str(len(candidatos)))
    
    candidatos = remove_candidatos(candidatos)  
    
    print('-------------')
    candidatos = confere(trajs, candidatos, d)
    print('pontos candidatos apos conferencia de melhores pontos: ' + str(len(candidatos)))
    
    imprime_candidatos(candidatos)
    
    
    fim = time.time()
    intervalo = fim - inicio
    
    print('calculou em: ' + str(intervalo) + ' s')
    
    
    #output_file(saida,candidatos)
    
    file_name = 'output_traclus'
    output_file_traclus(file_name,candidatos)
    
    return 0

if __name__ == '__main__':
    main()

